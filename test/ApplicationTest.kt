package com.assignment

import com.assignment.model.Account
import com.assignment.model.Transfer
import com.assignment.model.toAccount
import com.assignment.model.toJson
import io.ktor.application.Application
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.setBody
import io.ktor.server.testing.withTestApplication
import kotlin.test.Test
import kotlin.test.assertEquals

class ApplicationTest {

    @Test
    fun shouldTransferMoneyBetweenAccounts() {
        withTestApplication(Application::module) {

            // given

            handleRequest(HttpMethod.Post, "/accounts") {
                addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                setBody(
                    Account(10, "investments", 5000.0).toJson()
                )
            }.apply {
                assertEquals(HttpStatusCode.Created, response.status())
            }
            handleRequest(HttpMethod.Post, "/accounts") {
                addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                setBody(
                    Account(20, "savings", 1000.0).toJson()
                )
            }.apply {
                assertEquals(HttpStatusCode.Created, response.status())
            }

            // when

            handleRequest(HttpMethod.Post, "/transfers") {
                addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                setBody(
                    Transfer(10, 20, 1000.0).toJson()
                )
            }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
            }

            // then

            handleRequest(HttpMethod.Get, "/accounts/10").apply {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals(
                    Account(10, "investments", 6000.0),
                    response.content?.toAccount()
                )
            }
            handleRequest(HttpMethod.Get, "/accounts/20").apply {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals(
                    Account(20, "savings", 0.0),
                    response.content?.toAccount()
                )
            }
        }
    }
}