package com.assignment

import com.assignment.model.Account
import com.assignment.model.Transfer
import com.assignment.repository.Database
import com.assignment.repository.Repository
import com.fasterxml.jackson.databind.SerializationFeature
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing

fun Application.module() {

    Database.init()
    val repository = Repository()

    install(DefaultHeaders)
    install(CallLogging)
    install(ContentNegotiation) {
        jackson {
            configure(SerializationFeature.INDENT_OUTPUT, true)
        }
    }
    install(StatusPages) {
        exception<Throwable> {
            call.respond(HttpStatusCode.InternalServerError)
        }
    }

    routing {
        get("/accounts") {
            call.respond(repository.findAll())
        }
        get("/accounts/{id}") {
            val id = call.parameters["id"]!!.toInt()
            call.respond(repository.get(id))
        }
        post("/accounts") {
            val account = call.receive<Account>()
            call.respond(HttpStatusCode.Created, repository.create(account))
        }
        post("/transfers") {
            val transfer = call.receive<Transfer>()
            repository.process(transfer)
            call.respond(HttpStatusCode.OK)
        }
    }
}


