package com.assignment.model

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

data class Transfer(
    val receiverId: Int,
    val senderId: Int,
    val amount: Double
)

fun Transfer.toJson(): String = jacksonObjectMapper().writeValueAsString(this)
fun String.toTransfer(): Transfer = jacksonObjectMapper().readValue(this)