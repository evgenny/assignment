package com.assignment.model

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

data class Account(
    val id: Int,
    val name: String,
    val amount: Double
)

fun Account.toJson(): String = jacksonObjectMapper().writeValueAsString(this)
fun String.toAccount(): Account = jacksonObjectMapper().readValue(this)