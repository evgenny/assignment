package com.assignment.repository

import org.jetbrains.exposed.sql.Table

object AccountTable : Table() {
    val id = integer("id").autoIncrement()
    val amount = double("amount")
    val name = varchar("name", 255)
    override val primaryKey = PrimaryKey(id)
}
