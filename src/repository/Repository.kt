package com.assignment.repository

import com.assignment.model.Account
import com.assignment.model.Transfer
import com.assignment.repository.Database.dbQuery
import org.jetbrains.exposed.sql.*

class Repository {

    suspend fun findAll(): List<Account> = dbQuery {
        AccountTable.selectAll().map { toAccount(it) }
    }

    suspend fun get(id: Int): Account = dbQuery {
        AccountTable.select {
            AccountTable.id eq id
        }.map { toAccount(it) }
            .single()
    }

    suspend fun create(account: Account): Account {
        val id = dbQuery {
            AccountTable.insert {
                it[id] = account.id
                it[name] = account.name
                it[amount] = account.amount
            } get AccountTable.id
        }
        return get(id)
    }

    suspend fun process(transfer: Transfer) = dbQuery {
        AccountTable.update({ AccountTable.id eq transfer.receiverId }) {
            with(SqlExpressionBuilder) {
                it.update(amount, amount + transfer.amount)
            }
        }
        AccountTable.update({ AccountTable.id eq transfer.senderId }) {
            with(SqlExpressionBuilder) {
                it.update(amount, amount - transfer.amount)
            }
        }
    }

    private fun toAccount(row: ResultRow): Account {
        return Account(
            id = row[AccountTable.id],
            name = row[AccountTable.name],
            amount = row[AccountTable.amount]
        )
    }
}