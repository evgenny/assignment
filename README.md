## Backend Test Assignment

### Running the app:

`gradlew run` 

application will be available by `localhost:8080` address

### Routes:

`GET /accounts` --> get all accounts

`GET /accounts/{id}` --> get an account by id 

`POST /accounts` --> add a new account to the database by providing a JSON object. e.g - 

    {
        "name": "Savings",
        "amount": 1000.0
    }

returns

    {
        "id": 4,
        "name": "Savings",
        "amount": 1000.0
    }
    
`POST /transfers` --> transfer amount from sender to receiver account by providing a JSON object. e.g - 

    {
        "senderId": 1,
        "receiverId": 2,
        "amount": 1000.0
    }
    
### Libraries used:

 - [Ktor](https://github.com/ktorio/ktor) - Kotlin async web framework
 - [Netty](https://github.com/netty/netty) - Async web server
 - [Exposed](https://github.com/JetBrains/Exposed) - Kotlin SQL framework
 - [H2](https://github.com/h2database/h2database) - Embeddable database





